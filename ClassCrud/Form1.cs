﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MyClassDb;

namespace ClassCrud
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnadd_Click(object sender, EventArgs e)
        {
            ClassDb NewCon = new ClassDb();
            NewCon.connect();
            SqlCommand comm = new SqlCommand();
            comm.Connection = ClassDb.conn;

            comm.CommandText = "Insert into tblStu Values (@name, @gender, @phone)";
            comm.Parameters.AddWithValue("name", txtname);
            comm.Parameters.AddWithValue("gender", txtgender);
            comm.Parameters.AddWithValue("phone", txtphone);
            comm.ExecuteNonQuery(); 

            MessageBox.Show(txtname.Text + " Added.", "Added");
            txtname.Clear();
            txtgender.Clear();
            txtphone.Clear();
        }
    }
}
